
'use strict';

var webrtc = require('wrtc');

console.log('booting up webrtc client');

var payload = { candidates: [] };

var configuration = { iceServers: [{ urls: "stun:stun.l.google.com:19302" }]};
var pc = new webrtc.RTCPeerConnection(configuration);

function handleError(error) {
  throw error;
}

pc.onicecandidate = function(candidate) {
  if(!candidate.candidate) return;
  console.log('found candidate: ');
  payload.candidates.push(candidate.candidate);
  var json = JSON.stringify(payload);
  console.log(json);
};

pc.oniceconnectionstatechange = function(event) {
  var state = pc.iceConnectionState;
  console.log('ice state change: ' + state);
}

var dc;
pc.ondatachannel = function(event) {
  dc = event.channel;
  dc.onopen = function() {
    console.log('data channel open');
    dc.onmessage = function(event) {
      console.log('message received: ' + event.data);
    };
  };
};

demandInput('input offer: ', function(remotePayload) {
  console.log('parsing offer...');
  var offer = JSON.parse(remotePayload);
  pc.setRemoteDescription(offer.desc);
  for(var i = 0; i < offer.candidates.length; i++) {
    pc.addIceCandidate(offer.candidates[i]);
  }
  console.log('offer received. generating answer: ');
  pc.createAnswer(function(desc) {
    console.log('got local description: ');
    payload.desc = desc;
    var json = JSON.stringify(payload);
    console.log(json);
    pc.setLocalDescription(desc)
  }, handleError);
});

function demandInput(demand, inputHandler) {
  console.log(demand);
  var stdin = process.openStdin();
  var listener = function(d) {
    inputHandler(d.toString().trim());
    stdin.removeListener('data', listener);
  };
  stdin.addListener('data', listener);

}

