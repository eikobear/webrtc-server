
'use strict';

const WebSocket = require('ws');
const WebRTC = require('wrtc');

const sockets = [];
const peers = {};
const wss = new WebSocket.Server({ port: 8080 });

wss.on('connection', function connection(ws) {
  ws.on('message', function(message) { wsReadMessage(JSON.parse(message), ws) });
  sockets.push(ws);
  sendOffer(ws);
});

function wsReadMessage(message, ws) {
  console.log("message: " + message);
  if(message.desc) receiveAnswer(ws, message.desc);
  if(message.candidate) receiveCandidate(ws, message.candidate);
};


function sendOffer(ws) {
  var configuration = { iceServers: [{ urls: "stun:stun.l.google.com:19302" }]};
  var pc = new WebRTC.RTCPeerConnection(configuration);

  pc.onicecandidate = function(candidate) {
    if(!candidate.candidate) return;
    console.log('found candidate: ');
    var json_candidate = JSON.stringify(candidate);
    ws.send(json_candidate);
  };

  pc.oniceconnectionstatechange = function(event) {
    var state = pc.iceConnectionState;
    console.log('ice state change: ' + state);
  };

  var dc = pc.createDataChannel('test');

  dc.onopen = function() {
    console.log('data channel open');
    dc.onmessage = function(event) {
      console.log('message received: ' + event.data);
    };
  };
  

  console.log('creating offer');
  pc.createOffer(function(desc) {
    console.log('got local description: ');
    pc.setLocalDescription(desc);
    var json_desc = JSON.stringify({ desc: desc });
    ws.send(json_desc);
  }, handleError);

  peers[ws] = pc;
};

function receiveAnswer(ws, desc) {
  console.log('received answer: ' + desc);
  peers[ws].setRemoteDescription(desc);
}

function receiveCandidate(ws, candidate) {
  console.log('received candidate: ' + candidate);
  peers[ws].addIceCandidate(candidate);
}

function handleError(error) {
  throw error;
}







//demandInput('input answer to offer: ', function(remotePayload) {
//  var answer = JSON.parse(remotePayload);
//  pc.setRemoteDescription(answer.desc);
//  for(var i = 0; i < answer.candidates.length; i++) {
//    pc.addIceCandidate(answer.candidates[i]);
//  }
//  console.log('answer received');
//});



function demandInput(demand, inputHandler) {
  console.log(demand);
  var stdin = process.openStdin();
  var listener = function(d) {
    inputHandler(d.toString().trim());
    stdin.removeListener('data', listener);
  };
  stdin.addListener('data', listener);

}

