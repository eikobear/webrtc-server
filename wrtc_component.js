
'use strict';

var webrtc = require('wrtc');

var pc = new webrtc.RTCPeerConnection({iceServers: [

	{urls: 'stun:stun1.l.google.com:19302'},
	{urls: 'stun:stun2.l.google.com:19302'}

]})

pc.createDataChannel("webrtchacks")

pc.onicecandidate = function(e) {
	if (e.candidate && e.candidate.candidate.indexOf('srflx') !== -1) {
		var cand = e.candidate.candidate;
		if (!candidates[cand.relatedPort]) candidates[cand.relatedPort] = [];
		candidates[cand.relatedPort].push(cand.port);
	} else if (!e.candidate) {
		if (Object.keys(candidates).length === 1) {
			var ports = candidates[Object.keys(candidates)[0]];
			console.log(ports.length === 1 ? 'cool nat' : 'symmetric nat');

		}
	}
};

pc.createOffer()
.then(offer => pc.setLocalDescription(offer))
