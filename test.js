
let ws;
let pc;

function WebRTCHandshake() {
  ws = new WebSocket("ws://127.0.0.1:8080/handshake");
  ws.onmessage = function(event) {
    var d = JSON.parse(event.data);
    if(d.desc) receiveOffer(d.desc);
    if(d.candidate) receiveCandidate(d.candidate);
  };
};

function WebRTCSendInputData(inputData) {
  console.log('sending data: ' + inputData);
};

function receiveOffer(desc) {
  console.log('received description: ' + desc);
  var configuration = { iceServers: [{ urls: "stun:stun.l.google.com:19302" }]};
  pc = new RTCPeerConnection(configuration);

  pc.onicecandidate = function(candidate) {
    if(!candidate.candidate) return;
    console.log('found candidate: ');
    console.log(candidate);
    var json_candidate = JSON.stringify({ candidate: candidate.candidate } );
    console.log("sending candidate: ");
    console.log(json_candidate);
    ws.send(json_candidate);
  };

  pc.oniceconnectionstatechange = function(event) {
    var state = pc.iceConnectionState;
    console.log('ice state change: ' + state);
  }

  var dc;
  pc.ondatachannel = function(event) {
    dc = event.channel;
    dc.onopen = function() {
      console.log('data channel open');
      dc.onmessage = function(event) {
        console.log('message received: ' + event.data);
      };
    };
  };

  pc.setRemoteDescription(desc);
  pc.createAnswer(function(desc) {
    var json_desc = JSON.stringify({ desc: desc });
    ws.send(json_desc);
    pc.setLocalDescription(desc);
  }, handleError);
}

function receiveCandidate(candidate) {
  console.log('received candidate: ' + candidate);
  pc.addIceCandidate(candidate);
}

function handleError(error) {
  throw error;
}
